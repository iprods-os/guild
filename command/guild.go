package command

import (
	"github.com/mitchellh/cli"
	"github.com/posener/complete"
	"gitlab.com/iprods-os/guild/task"
)

type GuildCommand struct {
	Description                   string
	Command                       task.Command
	UI                            cli.Ui
	AutocompleteArgsPredictor     complete.Predictor
	AutocompleteFlagsPredictorMap complete.Flags
}

var defaultAutocompleteArgs = complete.PredictFiles("*")
var defaultAutocompleteFlags = complete.Flags{}

func (c *GuildCommand) Run(args []string) int {
	return c.Command.Run(args)
}

func (c *GuildCommand) Help() string {
	return ""
}

func (c *GuildCommand) Synopsis() string {
	return c.Description
}

func (c *GuildCommand) AutocompleteArgs() complete.Predictor {
	if c.AutocompleteArgsPredictor == nil {
		c.AutocompleteArgsPredictor = defaultAutocompleteArgs
	}
	return c.AutocompleteArgsPredictor
}

func (c *GuildCommand) AutocompleteFlags() complete.Flags {
	if c.AutocompleteFlagsPredictorMap == nil {
		c.AutocompleteFlagsPredictorMap = defaultAutocompleteFlags
	}
	return c.AutocompleteFlagsPredictorMap
}
