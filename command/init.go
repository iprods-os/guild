package command

import (
	"fmt"
	"strings"

	"github.com/mitchellh/cli"
	"gitlab.com/iprods-os/guild/task"
)

type InitCommand struct {
	UI cli.Ui
}

func (c *InitCommand) Run(_ []string) int {
	name, _ := c.UI.Ask("What is the name of the project?")
	if task.RunInit(name) > 0 {
		c.UI.Error(fmt.Sprintf("Failed creating Guildfile for project %s", name))
		return 1
	}
	c.UI.Info(fmt.Sprintf("Successfully created Guildfile for project %s", name))
	return 0
}

func (c *InitCommand) Help() string {
	helpText := `
Usage: guild init
	This command scaffolds a new guild project
	`
	return strings.TrimSpace(helpText)
}

func (c *InitCommand) Synopsis() string {
	return "Initiates a guild project"
}
