# guild

![build status](https://gitlab.com/iprods-os/guild/badges/master/build.svg) 
![code coverage](https://gitlab.com/iprods-os/guild/badges/master/coverage.svg)

Latest release  

![build status](https://gitlab.com/iprods-os/guild/badges/0.6.1/build.svg) 
![code coverage](https://gitlab.com/iprods-os/guild/badges/0.6.1/coverage.svg)

`guild` is a simple command wrapper written in Go. 

It aims to enable developers to configure tedious and hard to remember commands (or prefixes for commands).

It was mainly developed out of the needs when developing applications with `Docker` as a lot of the needed commands 
tend to be very long. It has similarities to `make` and `ant` which did not fit the needs of the development team.

`guild` is being used on a daily basis by 10+ developers doing frontend, backend and mobile development. The current
feature set can be regarded quite stable.

## Installation

After download it is recommended to copy the binary for your platform to a `PATH` reachable directory (e.g. 
`/usr/local/bin` on macOS).

>The binary should be renamed to `guild`. The download name has to be unique for each platform and so contains
a postfix.

To download the current release (0.6.1) please use:

* [macOS](https://gitlab.com/iprods-os/guild/-/jobs/artifacts/0.6.1/download?job=release-macos)
* [linux](https://gitlab.com/iprods-os/guild/-/jobs/artifacts/0.6.1/download?job=release-linux)

### CLI autocomplete

Autocomplete support for `Guild` is included out of the box. It is supported for `bash`, `zsh` and `fish`. 

To enable it, run:

```
guild -autocomplete-install
```

to disable, run

```
guild -autocomplete-uninstall
```

>After (un-)installation you need to source your shell env or start a new shell.

The autocomplete function will autocomplete all defined tasks.


## Usage

* Download the binary (or build it via `go build`), see Installation
* Create a `Guildfile` (this is based on TOML)
* Configure commands

## Configuration

`guild` has the following types and characteristics of tasks:

* normal tasks like f.e. a rather complex command call including args and flags that can be directly called
* normal tasks marked as internal that cannot be called directly but as dependent tasks
* inline scripts that are written as multiline strings with hashbang etc. pp.
* tasks that are based on a template

Besides that there is some project wide metadata that can be provided.

>Note: Guild (TOML) is case insensitive regarding the keys. This means `name` == `Name` == `NAME`. 
Further TOML is indentation agnostic. The examples shown here use lowercase keys and no indentation but feel free to 
use your own style.

### Project level

On the project level you have the possibility to set:

* `project`: The name of the project. This is displayed in the help.
* `vars`: Key value pairs that can be used as replacements in task commands.

### Normal task

```toml
project = "my-project"
[[task]]
name = "the-name-without-spaces"
command = "./the-command-to-run"
description = "The description of the task"
```

When running `guild the-name-without-spaces` the command `./the-command-to-run` will be run. 

If you use it as prefix when running f.e. `guild the-name-without-spaces my args --etc` those will get attached to the 
command and  `./the-command-to-run my args --etc` will be executed.

>Note that the arguments and flags do not inherit to dependent tasks.

>You can also use command substitutions like \`pwd\` or $(pwd) in the command.

>Shell operators like `&&`, `|` etc. are not allowed in normal tasks. See inline scripts for more.

### Variables

```toml
project = "my-project"

[vars]
my_var = "my-value"

[[task]]
name = "the-name-without-spaces"
command = "./the-command-to-run %(my_var)"
description = "The description of the task"
```

The command called will then be: `./the-command-to-run my-value`

This feature is helpful when there are some settings that should be more prominently placed or do change more often.

### Inline scripts

If you have more complex tasks with shell operators like pipes you can use inline scripts:

```toml
[[task]]
name = "inline-script"
command = """
    #!/usr/bin/env sh
    
    if [ $# -eq 0 ]; then
        echo "No arguments supplied"
    fi
    echo "Hello world"
    if [ "$1" != "" ]; then
        echo $1
    fi
    echo "Yoyo some words" | wc -l
    exit 0
    """
description = "The description of the task"
```

>Note that the indentation on the line start will be normalized to allow additional indentation for better readability.
This might cause issues though if you use an indentation based script language like Ruby or Python and screw up tabs 
vs. spaces.    

>Note that you have to add the hashbang part.

>Without hashbang the command will get concatenated to a single line (separated by a space character).

### Dependent tasks

Dependent tasks are tasks that will be executed *before* the called task. This allows to chain different tasks.

>This is per se not a separate task type but a different usage of tasks.

```toml
[[task]]
name = "the-name-without-spaces"
command = "./the-command-to-run"
description = "The description of the task"
depends = [ "some-other-task", "more args" ]

[[task]]
name = "some-other-task"
command = "./the-other-command-to-run"

[[task]]
name = "more"
command = "./something"
```

It is possible to pass additional arguments to depending tasks. These will get attached to the command of the 
depending task. It is **not** possible to pass in dynamic arguments f.e. from the CLI call.
 
>Warning: This feature has no measures detecting circular references.

### Internal tasks

Sometimes tasks are only needed to be re-used in other tasks but not really meant to be ran on their own.
For this scenario `guild` has a flag to indicate internal usage. Tasks having this set cannot be called. See also the
section about dependent tasks.

```toml
[[task]]
name = "the-name-without-spaces"
command = "./the-command-to-run"
description = "The description of the task"
depends = [ "some-other-task" ]

[[task]]
name = "some-other-task"
internal = true
command = "./the-other-command-to-run"
```

The `some-other-task` is not callable from the outside but can be used as depending task. It will also not be listed 
in the help message.

### Environment variables

As the commands are ran in a different context you sometimes want to have certain variables available when running your
command.

To do so please enclose the environment variable name with `%env=NAME%`. Then the original value will be taken.

```toml
[[task]]
name = "build-linux"
command = "GOPATH=%env:GOPATH% GOOS=linux GOARCH=386 CGO_ENABLED=0 go build -o build/linux/guild -v"
description = "Build guild for Linux"
```

### Platform dependent tasks

If you or your team are working on multiple platforms there arises the need to have platform depending tasks 
e.g. different paths on Linux or macOS. With the `platform` setting in `guild` you can restrict tasks to only be run on
a certain platform. So far only `linux` and `darwin` are tested.

```toml
[[task]]
name = "install"
command = "echo Installed guild"
description = "Install guild on from the release folder"
depends = [ "install-linux", "install-macos" ]

[[internal]]
name = "install-macos"
platform = "darwin"
command = "cp build/guild /usr/local/bin"

[[internal]]
name = "install-linux"
platform = "linux"
command = "cp build/guild ."
```

In the upper example when being on macOS and running the `install` task the Linux task will not be performed 
(though a skip message will be displayed).

### Template tasks

There are cases where you need to have many slightly different tasks. Those are long but differ only on a portion of
the command. Therefore `guild` has template support where you can define multiple commands with the help of a command.

```toml
[[template]]
name = "c"
description = "Command"
template = "ls %s"
[[template.task]]
name = "ll"
replacements = [ "-la" ]
description = "additional information about this task"
[[template.task]]
name = "lr"
replacements = [ "-laR" ]
```

The command will be expanded to two commands in this case: `c:ll` and `c:lr`. Additionally there will be a parent task
`c` available that will run all expanded tasks.

Besides this behavior you can also scope template tasks to be internal (not callable or shown in help) tasks. Therefore
you can either make them completely private by putting `internal = true` to the task description or put the 
`internal = true` to a sub-command section to only make those private.

If you need to have the raw output of the wrapped command you can use the `quiet = true` setting for the respective 
expanded task. Alternatively you can "quiet" the template with all commands.

```toml
[[template]]
name = "c"
description = "Command"
internal = true # Will hide all tasks including the top `c` task
template = "ls %s"
[[template.command]]
name = "ll"
replacements = [ "-la" ]
description = "additional information about this task"
[[template.command]]
name = "la"
replacements = [ "-laR" ]

[[template]]
name = "d"
description = "Command"
template = "ls %s"
[[template.command]]
name = "ll"
replacements = [ "-la" ]
internal = true # Will hide only this templated version
[[template.command]]
name = "lr"
replacements = [ "-laR" ]
description = "additional information about this task"
```

If you need to replace one string multiple time in f.e. an inline script you can use explicit argument indexes:
```toml
[[template]]
name = "c"
description = "Command"
internal = true # Will hide all tasks including the top `c` task
template = """
#!/usr/bin/env sh
echo Going to run some-command %[1]s 
some-command %[1]s
echo Successfully ran some-command %[1]s
"""
[[template.command]]
name = "a"
replacements = [ "argument-a" ]
description = "additional information about this task"
[[template.command]]
name = "b"
replacements = [ "argument-b" ]
```

### Tasks with confirmation

If you have some expensive or even risky to dangerous tasks there is the possibility to ask the user for additional 
confirmation.

In order to do so you can use the `confirm = "Do you want to do this?"` option. This is applicable to all public tasks.
Internal tasks do not use this setting (as of now). It will also be asked if the task is called by another task.

```toml
[[task]]
name = "a"
command = "command-a"
confirm = "Really?"
description = "Command A"

[[template]]
name = "c"
description = "Command"
confirm = "Sure or not?"
template = "some-longer %s command %s"
[[template.task]]
name = "a"
confirm = "Sure?"
replacements = [ "PreA", "PostA" ]
[[template.task]]
name = "b"
confirm = "Are you really sure?"
replacements = [ "PreB", "PostB" ]

```

In the example the question will occur on all tasks: `a`, `c`, `c:a`, `c:b`

## Development

* For local dev first make sure Go is properly installed, including setting up a [`GOPATH`](http://golang.org/doc/code.html#GOPATH).
* Clone this repo to `$GOPATH/src/gitlab.com/iprods-os/guild`

> Add how to install dependencies.

### OSX

Install Go with cross compilers

```
brew install go --with-cc-common
```

## Roadmap

* Improve dependent tasks
    * Detect circular references (e.g. self referencing)
* Add `Guildfile` validation (e.g. whitespace in the name is a no go, guild tasks are excluded and user gets warned when 
using)

## License

MIT
