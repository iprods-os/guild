module gitlab.com/iprods-os/guild

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/armon/go-radix v1.0.0
	github.com/bgentry/speakeasy v0.1.0
	github.com/hashicorp/errwrap v1.0.0
	github.com/hashicorp/go-multierror v1.0.0
	github.com/mattn/go-isatty v0.0.4
	github.com/mitchellh/cli v1.0.0
	github.com/posener/complete v1.2.1
	golang.org/x/sys v0.0.0-20181026203630-95b1ffbd15a5
)
