package main

import (
	"os"

	"gitlab.com/iprods-os/guild/cli"
)

func main() {
	os.Exit(cli.Run(os.Args[1:]))
}
