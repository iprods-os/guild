package task

import (
	"testing"

	specs "gitlab.com/iprods-os/guild/testing"
)

type MockLoader struct {
	definitions Definitions
}

func (l *MockLoader) LoadFile() (Definitions, error) {
	return l.definitions, nil
}

func Test_returns_an_error_if_parsing_failed(t *testing.T) {
	spec := specs.SpecTest(t)
	tasks := []task{{
		Name:    "task_name",
		Command: "task_command",
	}, {
		Name:    "task_name",
		Command: "task_command",
	}}
	definitions := Definitions{
		Project: "project_name",
		Task:    tasks,
	}
	sourceLoader := &MockLoader{
		definitions: definitions,
	}
	loader := Loader{
		DefinitionLoader: sourceLoader,
	}
	_, err := NewProject(loader)
	spec.Expect(err.Error()).ToEqual("duplicate task 'task_name' found")
}

func Test_creates_a_project(t *testing.T) {
	spec := specs.SpecTest(t)
	tasks := []task{{
		Name:    "task_name",
		Command: "task_command",
	}}
	definitions := Definitions{
		Project: "project_name",
		Task:    tasks,
	}
	sourceLoader := &MockLoader{
		definitions: definitions,
	}
	loader := Loader{
		DefinitionLoader: sourceLoader,
	}
	project, err := NewProject(loader)
	spec.Expect(err).ToEqual(nil)
	spec.Expect(project.Name).ToEqual("project_name")
	spec.Expect(len(project.Tasks)).ToEqual(1)
	spec.Expect(project.Tasks["task_name"].Name).ToEqual("task_name")
	spec.Expect(project.Tasks["task_name"].Command).ToEqual("task_command")
}

func Test_it_replaces_project_variables(t *testing.T) {
	spec := specs.SpecTest(t)
	tasks := []task{{
		Name:    "task_name",
		Command: "task_command %(replace) %(replace_also)",
	}}
	vars := map[string]string{"replace": "me", "replace_also": "too"}
	definitions := Definitions{
		Project: "project_name",
		Vars:    vars,
		Task:    tasks,
	}
	sourceLoader := &MockLoader{
		definitions: definitions,
	}
	loader := Loader{
		DefinitionLoader: sourceLoader,
	}
	project, err := NewProject(loader)
	spec.Expect(err).ToEqual(nil)
	spec.Expect(len(project.Tasks)).ToEqual(1)
	spec.Expect(project.Tasks["task_name"].Name).ToEqual("task_name")
	spec.Expect(project.Tasks["task_name"].Command).ToEqual("task_command me too")
}

var parseTests = []struct {
	name            string
	tasks           []task
	internal        []task
	template        []template
	expectedProject Project
}{
	{
		name: "full_task",
		tasks: []task{{
			Name:        "task",
			Command:     "command",
			Description: "description",
			Internal:    true,
			Platform:    "darwin",
			Confirm:     "confirm",
			Depends:     []string{},
		}},
		expectedProject: Project{
			Name: "full_task",
			Tasks: map[string]Task{
				"task": {
					Name:        "task",
					Command:     "command",
					Description: "description",
					Internal:    true,
					Platform:    "darwin",
					Confirm:     "confirm",
					Depends:     []string{},
				},
			},
		},
	},
	{
		name: "full_task_with_dependent",
		tasks: []task{{
			Name:        "task",
			Command:     "command",
			Description: "description",
			Internal:    true,
			Platform:    "darwin",
			Confirm:     "confirm",
			Depends:     []string{"task"},
		}},
		expectedProject: Project{
			Name: "full_task_with_dependent",
			Tasks: map[string]Task{
				"task": {
					Name:        "task",
					Command:     "command",
					Description: "description",
					Internal:    true,
					Platform:    "darwin",
					Confirm:     "confirm",
					Depends:     []string{"task"},
				},
			},
		},
	},
	{
		name: "full_internal",
		internal: []task{{
			Name:        "task",
			Command:     "command",
			Description: "description",
			Platform:    "darwin",
			Confirm:     "confirm",
			Depends:     []string{},
		}},
		expectedProject: Project{
			Name: "full_internal",
			Tasks: map[string]Task{
				"task": {
					Name:        "task",
					Command:     "command",
					Description: "description",
					Internal:    true,
					Platform:    "darwin",
					Confirm:     "confirm",
					Depends:     []string{},
				},
			},
		},
	},
	{
		name: "full_template",
		template: append([]template{}, template{
			Name:        "template",
			Template:    "do %s",
			Description: "description",
			Platform:    "darwin",
			Internal:    true,
			Confirm:     "confirm",
			Depends:     []string{},
			Task: []templateTask{{
				Name:         "task-1",
				Description:  "description-1",
				Internal:     true,
				Confirm:      "confirm-1",
				Replacements: []interface{}{"task 1"},
			}, {
				Name:         "task-2",
				Description:  "description-2",
				Internal:     true,
				Confirm:      "confirm-2",
				Replacements: []interface{}{"task 2"},
			}},
		}),
		expectedProject: Project{
			Name: "full_template",
			Tasks: map[string]Task{
				"template:task-1": {
					Name:        "template:task-1",
					Command:     "do task 1",
					Description: "description description-1",
					Internal:    true,
					Platform:    "darwin",
					Confirm:     "confirm-1",
					Depends:     []string{},
				},
				"template:task-2": {
					Name:        "template:task-2",
					Command:     "do task 2",
					Description: "description description-2",
					Internal:    true,
					Platform:    "darwin",
					Confirm:     "confirm-2",
					Depends:     []string{},
				},
				"template": {
					Name:        "template",
					Command:     "echo Ran template",
					Description: "description",
					Internal:    true,
					Platform:    "darwin",
					Confirm:     "confirm",
					Depends:     []string{"template:task-1", "template:task-2"},
				},
			},
		},
	},
	{
		name: "full_template_with_dependent",
		template: append([]template{}, template{
			Name:        "template",
			Template:    "do %s",
			Description: "description",
			Platform:    "darwin",
			Internal:    true,
			Confirm:     "confirm",
			Depends:     []string{"depend"},
			Task: []templateTask{{
				Name:         "task-1",
				Description:  "description-1",
				Internal:     true,
				Confirm:      "confirm-1",
				Replacements: []interface{}{"task 1"},
			}, {
				Name:         "task-2",
				Description:  "description-2",
				Internal:     true,
				Confirm:      "confirm-2",
				Replacements: []interface{}{"task 2"},
			}},
		}),
		expectedProject: Project{
			Name: "full_template_with_dependent",
			Tasks: map[string]Task{
				"template:task-1": {
					Name:        "template:task-1",
					Command:     "do task 1",
					Description: "description description-1",
					Internal:    true,
					Platform:    "darwin",
					Confirm:     "confirm-1",
					Depends:     []string{"depend"},
				},
				"template:task-2": {
					Name:        "template:task-2",
					Command:     "do task 2",
					Description: "description description-2",
					Internal:    true,
					Platform:    "darwin",
					Confirm:     "confirm-2",
					Depends:     []string{"depend"},
				},
				"template": {
					Name:        "template",
					Command:     "echo Ran template",
					Description: "description",
					Internal:    true,
					Platform:    "darwin",
					Confirm:     "confirm",
					Depends:     []string{"depend", "template:task-1", "template:task-2"},
				},
			},
		},
	},
}

func Test_it_creates_a_project_from_definitions(t *testing.T) {
	spec := specs.SpecTest(t)
	for _, tt := range parseTests {
		definitions := Definitions{
			Project:  tt.name,
			Task:     tt.tasks,
			Internal: tt.internal,
			Template: tt.template,
		}
		sourceLoader := &MockLoader{
			definitions: definitions,
		}
		loader := Loader{
			DefinitionLoader: sourceLoader,
		}
		project, err := NewProject(loader)
		spec.Expect(err).ToEqual(nil)
		spec.Expect(project.Name).ToEqual(tt.name)
		// Add the internal definitions field
		tt.expectedProject.definitions = definitions
		spec.Expect(*project).ToEqual(tt.expectedProject)
		if !t.Failed() {
			t.Logf("\u2713\t%s", tt.name)
		}
	}
}
