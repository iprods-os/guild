package task

import "fmt"

// Create an error
func e(format string, args ...interface{}) error {
	return fmt.Errorf(format, args...)
}
