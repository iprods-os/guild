package task

import (
	"os"
	"regexp"
	"strings"
)

var shellOperators = []struct {
	literal string
}{
	{";"},
	{"&&"},
	{"&"},
	{"||"},
	{"|"},
}

// The builder that builds a runnable command out of a task definition.
type Builder struct {
	Project  Project            // The project with the tasks defined
	Commands map[string]Command // The generated commands
}

// Create a new builder for a given project.
func NewBuilder(project Project) (*Builder, error) {
	builder := &Builder{
		Project: project,
	}
	builder.Commands = make(map[string]Command)
	for name, taskDefinition := range builder.Project.Tasks {
		if _, ok := builder.Commands[name]; ok {
			// command was already created via a depends resolution
			continue
		}
		command, err := builder.buildCommand(taskDefinition)
		if err != nil {
			return builder, err
		}
		builder.Commands[name] = command
	}

	return builder, nil
}

// Build the command with the dependent tasks.
func (b *Builder) buildCommand(taskDefinition Task) (Command, error) {
	commandName, commandArgs, commandEnvVars, inlineScript, err := splitCommandString(taskDefinition.Command)
	if err != nil {
		return Command{}, err
	}
	var preChain []Command
	if len(taskDefinition.Depends) > 0 {
		for _, taskCall := range taskDefinition.Depends {
			taskName, taskArgs, taskEnvVars, _, err := splitCommandString(taskCall)
			if err != nil {
				return Command{}, err
			}
			if _, ok := b.Project.Tasks[taskName]; !ok {
				return Command{}, e("dependent task '%s' does not exist which was defined in '%s'", taskName, taskDefinition.Name)
			}
			preCommand, err := b.buildCommand(b.Project.Tasks[taskName])
			if err != nil {
				return Command{}, err
			}
			b.Commands[taskName] = preCommand
			preCommand.Args = append(preCommand.Args, taskArgs...)
			preCommand.Env = append(preCommand.Env, taskEnvVars...)
			preChain = append(preChain, preCommand)
		}
	}
	return Command{
		Name:         commandName,
		Args:         commandArgs,
		Env:          commandEnvVars,
		Internal:     taskDefinition.Internal,
		Description:  taskDefinition.Description,
		Confirmation: taskDefinition.Confirm,
		Platform:     taskDefinition.Platform,
		preChain:     preChain,
		inlineScript: inlineScript,
	}, nil
}

// Separate the Command to run and it's additional args and flags.
func splitCommandString(taskCommand string) (string, []string, []string, bool, error) {
	// Multiline command is present so treat it differently
	if strings.Contains(taskCommand, "\n") {
		var lines []string
		for _, line := range strings.Split(taskCommand, "\n") {
			if strings.TrimSpace(line) == "" {
				continue
			}
			lines = append(lines, line)
		}
		// Detect hashbang which will then be treated as a script
		if strings.Index(lines[0], "#!") != -1 {
			// Remove additional indentation; therefore take the indentation of the first line and replace at the
			// beginning of each following line
			re := regexp.MustCompile(`^\s*`)
			indent := re.FindString(lines[0])
			var scriptLines []string
			for _, line := range lines {
				line = strings.Replace(line, indent, "", 1)
				scriptLines = append(scriptLines, line)
			}
			script := strings.Join(scriptLines, "\n")
			// The command is a script; there are neither arguments or env vars possible
			// Return the script with the flag indicating to treat this command later as a script
			return script, []string{}, []string{}, true, nil
		}
		// If it's not a script it will be combined to one line and treated as a normal single command with arguments
		taskCommand = strings.Join(lines, " ")
	}
	if err := checkForShellOperators(taskCommand); err != nil {
		return "", []string{}, []string{}, false, err
	}
	taskCommand = strings.TrimSpace(taskCommand)
	// TODO What happens if it's an empty string?
	var parts []string
	for _, p := range strings.Split(taskCommand, " ") {
		parts = append(parts, strings.TrimSpace(p))
	}
	var nameIndex int
	var envVars []string
	for index, part := range parts {
		// TODO Check if command env detection is sufficient
		if !strings.Contains(part, "=") {
			nameIndex = index
			break
		}
		envVars = append(envVars, resolveEnvironmentVariable(part))
	}
	command := parts[nameIndex]
	var args []string
	if len(parts) == nameIndex {
		return command, args, envVars, false, nil
	}
	args = parts[nameIndex+1:]
	return command, args, envVars, false, nil
}

// Resolve environment variables with and without placeholders.
func resolveEnvironmentVariable(part string) string {
	if strings.Contains(part, "%env:") {
		envVarParts := strings.Split(part, "=")
		envKey := strings.Replace(envVarParts[1], "%", "", 2)
		envKey = strings.Replace(envKey, "env:", "", 1)
		return envVarParts[0] + "=" + os.Getenv(envKey)
	}
	return part
}

// Detect shell operators.
func checkForShellOperators(command string) error {
	for _, operator := range shellOperators {
		if strings.Contains(command, operator.literal) {
			return e("shell operators are not supported; detected \"%s\" in \"%s\"", operator.literal, command)
		}
	}
	return nil
}
