package task

// The structs here represent the Guildfile possibilities.

// A template templateTask will be expanded to multiple tasks.
type template struct {
	Name        string         // The task group name
	Internal    bool           // The flag whether the main task is exposed to be callable
	Command     []templateTask // The real tasks {deprecated; use Task instead}
	Task        []templateTask // The real tasks (with replacements for the template; see templateTask)
	Confirm     string         // The confirmation question (implicit flag)
	Template    string         // The template with the placeholders being replaced by the task portions
	Description string         // The Description for the task group
	Platform    string         // The supported Platform
	Depends     []string       // The tasks that will be ran before
}

// The templateTask is the sub element of a template.
type templateTask struct {
	Name         string        // The sub task Name
	Replacements []interface{} // The replacements; Any scalars; TODO allow only strings?
	Confirm      string        // The confirmation question (implicit flag)
	Internal     bool          // The flag whether the main task is exposed to be callable
	Description  string        // The Description for the task
}

// This task structure for internal {deprecated} and normal tasks.
type task struct {
	Name        string   // The templateTask Name
	Command     string   // The wrapped templateTask
	Confirm     string   // The confirmation question (implicit flag)
	Description string   // The Description for the task
	Platform    string   // The supported Platform
	Depends     []string // The tasks that will be ran before
	Internal    bool     // The internal flag {deprecates Definitions.Internal} TODO !!BC check!!
}

// Definitions contains the Guildfile structure being loaded.
type Definitions struct {
	Project  string            // The project Name [optional]
	Vars     map[string]string // The project variables [optional]
	Task     []task            // The task definitions
	Internal []task            // The internal task definitions {deprecated}
	Template []template        // The templated task definitions
}

// SourceLoader is the interface for different file loaders. Those return the raw definitions.
type SourceLoader interface {
	LoadFile() (Definitions, error)
}

// Loader contains the source loader
type Loader struct {
	DefinitionLoader SourceLoader
}

// NewLoader creates a new loader.
func NewLoader(source string) *Loader {
	sourceLoader := Toml{
		Source: source,
	}
	return &Loader{
		DefinitionLoader: &sourceLoader,
	}
}

// Function to actually load the task definitions from a source.
func (l *Loader) Load() (Definitions, error) {
	definitions, err := l.DefinitionLoader.LoadFile()
	if err != nil {
		return definitions, e("loading tasks failed: %s", err.Error())
	}

	return definitions, nil
}
