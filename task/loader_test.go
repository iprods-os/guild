package task

import (
	"testing"

	specs "gitlab.com/iprods-os/guild/testing"
)

type NullLoader struct{}

func (l *NullLoader) LoadFile() (Definitions, error) {
	var definitions Definitions
	return definitions, nil
}

type FailingMockLoader struct{}

func (l *FailingMockLoader) LoadFile() (Definitions, error) {
	var definitions Definitions
	return definitions, e("utterly failed")
}

func Test_it_orchestrates_the_definition_loading(t *testing.T) {
	spec := specs.SpecTest(t)
	sourceLoader := new(NullLoader)
	loader := Loader{
		DefinitionLoader: sourceLoader,
	}
	definitions, err := loader.Load()
	expected := Definitions{}
	spec.Expect(err).ToEqual(nil)
	spec.Expect(definitions).ToEqual(expected)
}

func Test_it_throws_an_error_if_the_source_loader_reported_a_failure(t *testing.T) {
	spec := specs.SpecTest(t)
	sourceLoader := new(FailingMockLoader)
	loader := Loader{
		DefinitionLoader: sourceLoader,
	}
	_, err := loader.Load()
	spec.Expect(err.Error()).ToEqual("loading tasks failed: utterly failed")
}
