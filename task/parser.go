package task

import (
	"fmt"
	"strings"
)

// Task defines the main task structure. All other types are unfolded to this structure.
type Task struct {
	Name        string   // The templateTask Name
	Command     string   // The wrapped templateTask
	Confirm     string   // The confirmation question (implicit flag)
	Description string   // The Description for the task
	Platform    string   // The supported Platform
	Depends     []string // The tasks that will be ran before
	Internal    bool     // The internal flag {deprecates Definitions.Internal} TODO !!BC check!!
}

// Project defines the project with its tasks.
type Project struct {
	Name        string            // The project name [optional]
	Vars        map[string]string // The variables that can be used in the project defintion via placeholders
	Tasks       map[string]Task   // The task definitions
	definitions Definitions       // The internal list of the loaded definitions
}

// NewProject creates a new parser instance for the raw loaded definitions.
// The parser is used to convert the raw data, map internal tasks and expand the template tasks.
func NewProject(loader Loader) (*Project, error) {
	d, err := loader.Load()
	if err != nil {
		return &Project{}, err
	}

	project := &Project{
		definitions: d,
	}

	return project, project.parse()
}

// Parse loader provided definitions to a normalized structure.
// This structure will then be parsed to commands but also is used for autocompletion.
func (d *Project) parse() error {
	d.Vars = d.definitions.Vars
	var tasks []Task
	for _, t := range d.definitions.Task {
		c, err := d.replaceVariables(t.Command)
		if err != nil {
			return err
		}
		tasks = append(tasks, Task{
			Name:        t.Name,
			Command:     c,
			Description: t.Description,
			Confirm:     t.Confirm,
			Internal:    t.Internal,
			Platform:    t.Platform,
			Depends:     t.Depends,
		})
	}
	templateTasks, err := d.expandTemplates()
	if err != nil {
		return err
	}
	tasks = append(tasks, templateTasks...)
	// TODO Emit deprecation warning for Internal usage; Flag should be preferred.
	for _, t := range d.definitions.Internal {
		c, err := d.replaceVariables(t.Command)
		if err != nil {
			return err
		}
		tasks = append(tasks, Task{
			Name:        t.Name,
			Command:     c,
			Description: t.Description,
			Confirm:     t.Confirm,
			Internal:    true,
			Platform:    t.Platform,
			Depends:     t.Depends,
		})
	}
	d.Name = d.definitions.Project
	tasksMap := make(map[string]Task)
	for _, t := range tasks {
		if _, ok := tasksMap[t.Name]; ok {
			return e("duplicate task '%s' found", t.Name)
		}
		tasksMap[t.Name] = t
	}
	d.Tasks = tasksMap
	return nil
}

// Process a template with building all the defined concrete tasks.
func (d *Project) expandTemplates() ([]Task, error) {
	var tasks []Task
	if len(d.definitions.Template) == 0 {
		return tasks, nil
	}
	for _, templateTask := range d.definitions.Template {
		if len(templateTask.Command) > 0 {
			// TODO Emit deprecation warning for Command usage
			templateTask.Task = append(templateTask.Task, templateTask.Command...)
		}
		depends := templateTask.Depends
		// Build the real tasks
		for _, task := range templateTask.Task {
			task, err := d.buildTemplatedTask(templateTask, task)
			if err != nil {
				return tasks, err
			}
			depends = append(depends, task.Name)
			tasks = append(tasks, task)
		}
		// Add stub Command for the main template task
		templateStubCommand := "echo Ran " + templateTask.Name
		tasks = append(tasks, Task{
			Name:        templateTask.Name,
			Command:     templateStubCommand,
			Confirm:     templateTask.Confirm,
			Internal:    templateTask.Internal,
			Depends:     depends,
			Platform:    templateTask.Platform,
			Description: templateTask.Description,
		})
	}
	return tasks, nil
}

// Build a task from a template with its replacements.
func (d *Project) buildTemplatedTask(tmpl template, ta templateTask) (Task, error) {
	cmdName := ta.Name
	// Build Command and strip whitespace
	cmd := strings.TrimSpace(fmt.Sprintf(tmpl.Template, ta.Replacements...))
	taskName := tmpl.Name + ":" + cmdName
	description := tmpl.Description
	// Append additional Description to the templateTask Description
	if ta.Description != "" {
		description += " " + ta.Description
	}
	c, err := d.replaceVariables(cmd)
	if err != nil {
		return Task{}, nil
	}
	return Task{
		Name:        taskName,
		Command:     c,
		Confirm:     ta.Confirm,
		Internal:    ta.Internal,
		Depends:     tmpl.Depends,
		Platform:    tmpl.Platform,
		Description: description,
	}, nil
}

// replaceVariables replaces variables within commands
func (d *Project) replaceVariables(cmd string) (string, error) {
	if !strings.Contains(cmd, "%(") {
		return cmd, nil
	}
	found := false
	for name, value := range d.Vars {
		ph := "%(" + name + ")"
		if strings.Contains(cmd, ph) {
			cmd = strings.Replace(cmd, ph, value, -1)
			found = true
		}
	}
	if found {
		return cmd, nil
	}
	return "", e("variable in '%s' not found", cmd)
}
