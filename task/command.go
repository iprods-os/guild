package task

import (
	"bufio"
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"os/signal"
	"runtime"
	"strings"
)

// The runnable command.
type Command struct {
	Name         string    // The command to run
	Args         []string  // The command args
	Env          []string  // The environment settings
	Description  string    // The description of the command
	Internal     bool      // Flag to set the command to be executed only internally
	Confirmation string    // The confirmation string
	Platform     string    // The platform the command should exclusively be ran for
	preChain     []Command // The chain of commands being ran as dependent ones before
	inlineScript bool      // Flag to indicate if the command needs to be exported to a file
}

// Run the templateTask and return the return code.
func (c *Command) Run(args []string) int {
	if c.Platform != "" && c.Platform != runtime.GOOS {
		return 0
	}
	if !c.confirm() {
		return 1
	}
	// Run all dependant commands that should be ran before a command
	if len(c.preChain) > 0 {
		for _, pc := range c.preChain {
			code := pc.Run(args)
			if code != 0 {
				return 1
			}
		}
	}
	cleanup := false
	if c.inlineScript {
		// TODO Improve naming of the file
		hasher := md5.New()
		hasher.Write([]byte(c.Name))
		hash := hex.EncodeToString(hasher.Sum(nil))
		name := os.TempDir() + "/.guild_" + hash
		if err := ioutil.WriteFile(name, []byte(c.Name), 0700); err != nil {
			// TODO Convert to error
			panic(err)
		}
		// TODO Check if f.e. ctrl-c still removes the file
		defer os.Remove(name)
		cleanup = true
		c.Name = name
	}
	runArgs := append(c.Args, args...)
	cmd := exec.Command(c.Name, runArgs...)
	// Add the specific environment setting to the current ones
	cmd.Env = append(cmd.Env, c.Env...)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	registerIntercept(cmd, c.Name, cleanup)
	err := cmd.Run()
	if err != nil {
		return 1
	}
	return 0
}

// Intercept ctrl-c which will also send the signal to the started process.
func registerIntercept(cmd *exec.Cmd, name string, cleanup bool) {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		for sig := range c {
			// sig is a ^C, handle it
			cmd.Process.Signal(sig)
			// If it's an inline script we want to cleanup
			if cleanup {
				os.Remove(name)
			}
			os.Exit(130)
		}
	}()
}

// Dialog to get final confirmation if a templateTask should be run.
// This is useful for expensive and/or destructive commands.
func (c *Command) confirm() bool {
	if c.Confirmation == "" {
		return true
	}
	reader := bufio.NewReader(os.Stdin)
	for {
		fmt.Printf("%s [y/n]: ", c.Confirmation)
		response, err := reader.ReadString('\n')
		if err != nil {
			// TODO Handle error
		}
		response = strings.ToLower(strings.TrimSpace(response))
		if response == "y" || response == "yes" {
			return true
		}
		if response == "n" || response == "no" {
			return false
		}
	}
}
