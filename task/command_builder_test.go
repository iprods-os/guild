package task

import (
	"os"
	"sort"
	"testing"

	specs "gitlab.com/iprods-os/guild/testing"
)

func Test_returns_a_builder(t *testing.T) {
	NewBuilder(Project{})
}

var builderTests = map[string]struct {
	tasks            map[string]Task
	expectedCommands map[string]Command
}{
	"01 command building for task with command only": {
		map[string]Task{
			"task": {
				Name:    "task",
				Command: "command",
			},
		},
		map[string]Command{
			"task": {
				Name: "command",
				Args: []string{},
			},
		},
	},
	"02 command building for task with command, args and flags": {
		map[string]Task{
			"task": {
				Name:    "task",
				Command: "command with args --and-flags",
			},
		},
		map[string]Command{
			"task": {
				Name: "command",
				Args: []string{"with", "args", "--and-flags"},
			},
		},
	},
	"03 command building for task with env containing command": {
		map[string]Task{
			"task": {
				Name:    "task",
				Command: "GOPATH=%env:GOPATH% command",
			},
		},
		map[string]Command{
			"task": {
				Name: "command",
				Args: []string{},
				Env:  []string{"GOPATH=" + os.Getenv("GOPATH")},
			},
		},
	},
	"04 command building with dependent task": {
		map[string]Task{
			"task": {
				Name:    "task",
				Command: "command",
				Depends: []string{"depend"},
			},
			"depend": {
				Name:    "depend",
				Command: "command-depend",
			},
		},
		map[string]Command{
			"task": {
				Name: "command",
				Args: []string{},
				preChain: []Command{{
					Name: "command-depend",
					Args: []string{},
				}},
			},
			"depend": {
				Name: "command-depend",
				Args: []string{},
			},
		},
	},
	"05 command building for task with multiline command": {
		map[string]Task{
			"task": {
				Name: "task",
				Command: `command
					arg
					arg`,
			},
		},
		map[string]Command{
			"task": {
				Name: "command",
				Args: []string{"arg", "arg"},
			},
		},
	},
	"06 command building for task with inline script": {
		map[string]Task{
			"task": {
				Name: "task",
				Command: `
					#!/usr/bin/env
					echo a
				`,
			},
		},
		map[string]Command{
			"task": {
				Name:         "#!/usr/bin/env\necho a",
				inlineScript: true,
				Args:         []string{},
				Env:          []string{},
			},
		},
	},
	"07 command building for task with inline script and shell operators": {
		map[string]Task{
			"task": {
				Name: "task",
				Command: `
					#!/usr/bin/env
					echo a | do something
				`,
			},
		},
		map[string]Command{
			"task": {
				Name:         "#!/usr/bin/env\necho a | do something",
				inlineScript: true,
				Args:         []string{},
				Env:          []string{},
			},
		},
	},
}

func Test_it_builds_commands_of_tasks(t *testing.T) {
	spec := specs.SpecTest(t)
	var keys []string
	for k := range builderTests {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	for _, k := range keys {
		name := k
		tt := builderTests[k]
		project := Project{
			Tasks: tt.tasks,
		}
		builder, err := NewBuilder(project)
		spec.Expect(err).ToEqual(nil)
		spec.Expect(builder.Project).ToEqual(project)
		spec.Expect(len(builder.Commands)).ToEqual(len(tt.tasks))
		for k, actual := range builder.Commands {
			expected := tt.expectedCommands[k]
			spec.Expect(actual.Name).ToEqual(expected.Name)
			spec.Expect(actual.Platform).ToEqual(expected.Platform)
			spec.Expect(actual.Description).ToEqual(expected.Description)
			spec.Expect(actual.Internal).ToEqual(expected.Internal)
			spec.Expect(actual.Confirmation).ToEqual(expected.Confirmation)
			spec.Expect(actual.inlineScript).ToEqual(expected.inlineScript)
			spec.Expect(actual.Args).ToEqual(expected.Args)
			spec.Expect(actual.Env).ToEqual(expected.Env)
			spec.Expect(actual.preChain).ToEqual(expected.preChain)
		}
		if !t.Failed() {
			t.Logf("\u2713\t%s", name)
		}
	}
}

func Test_it_emits_an_error_if_shell_operators_are_used(t *testing.T) {
	spec := specs.SpecTest(t)
	project := Project{
		Tasks: map[string]Task{
			"task": {
				Name:    "task",
				Command: "command | command2",
			},
		},
	}
	_, err := NewBuilder(project)
	spec.Expect(err.Error()).ToEqual("shell operators are not supported; detected \"|\" in \"command | command2\"")
}
