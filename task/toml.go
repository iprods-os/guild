package task

import (
	"io/ioutil"

	"github.com/BurntSushi/toml"
)

type Toml struct {
	Source string
}

// Load and map the contents of a TOML file.
func (l *Toml) LoadFile() (Definitions, error) {
	config, err := ioutil.ReadFile(l.Source)
	var definitions Definitions
	if err != nil {
		return definitions, e("file '%s' does not exist", l.Source)
	}
	if _, err := toml.Decode(string(config), &definitions); err != nil {
		return definitions, e("file '%s' is malformed", l.Source)
	}
	return definitions, nil
}
