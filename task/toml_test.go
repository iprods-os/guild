package task

import (
	"testing"

	specs "gitlab.com/iprods-os/guild/testing"
)

// This is mainly some basic smoke and mirror testing for the loading as f.e. template is internal and loaded via
// reflection.

func Test_it_loads_an_empty_file(t *testing.T) {
	spec := specs.SpecTest(t)
	loader := Toml{
		Source: "fixtures/empty",
	}
	definitions, err := loader.LoadFile()
	spec.Expect(err).ToEqual(nil)
	spec.Expect(definitions).ToEqual(Definitions{})
}

func Test_it_loads_a_file_with_a_task_definition(t *testing.T) {
	spec := specs.SpecTest(t)
	loader := Toml{
		Source: "fixtures/task_definition",
	}
	definitions, err := loader.LoadFile()
	expectedTasks := append([]task{}, task{
		Name:        "task",
		Command:     "task-command",
		Description: "Task description",
	})
	spec.Expect(err).ToEqual(nil)
	spec.Expect(definitions).ToEqual(Definitions{
		Task: expectedTasks,
	})
}

func Test_it_loads_a_file_with_a_project_name(t *testing.T) {
	spec := specs.SpecTest(t)
	loader := Toml{
		Source: "fixtures/project_name",
	}
	definitions, err := loader.LoadFile()
	spec.Expect(err).ToEqual(nil)
	spec.Expect(definitions).ToEqual(Definitions{
		Project: "projectA",
	})
}

func Test_it_throws_an_error_if_the_file_is_not_present(t *testing.T) {
	spec := specs.SpecTest(t)
	loader := Toml{
		Source: "dontexist",
	}
	_, err := loader.LoadFile()
	spec.Expect(err.Error()).ToEqual("file 'dontexist' does not exist")
}

func Test_it_throws_an_error_if_the_file_content_is_malformed(t *testing.T) {
	spec := specs.SpecTest(t)
	loader := Toml{
		Source: "fixtures/malformed",
	}
	_, err := loader.LoadFile()
	spec.Expect(err.Error()).ToEqual("file 'fixtures/malformed' is malformed")
}
