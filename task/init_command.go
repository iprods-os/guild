package task

import (
	"bytes"
	"io/ioutil"
	"log"
	"os"

	"github.com/BurntSushi/toml"
)

// RunInit performs the initialization of a sample Guildfile.
func RunInit(name string) int {
	var template = map[string]interface{}{
		"project": name,
		"task": []Task{Task{
			Name:        "welcome",
			Command:     "echo Welcome to guild!",
			Description: "Print welcome message",
		}},
	}
	buf := new(bytes.Buffer)
	if err := toml.NewEncoder(buf).Encode(template); err != nil {
		log.Fatal(err)
		return 1
	}
	ioutil.WriteFile("Guildfile", buf.Bytes(), os.FileMode.Perm(0644))
	return 0
}
