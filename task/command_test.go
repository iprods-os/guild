package task

import (
	"testing"

	specs "gitlab.com/iprods-os/guild/testing"
)

func Test_it_runs_a_command(t *testing.T) {
	spec := specs.SpecTest(t)
	command := Command{
		Name: "echo",
	}
	code := command.Run([]string{})
	spec.Expect(code).ToEqual(0)
}

func Test_it_skips_an_unsupported_platform(t *testing.T) {
	spec := specs.SpecTest(t)
	command := Command{
		Platform: "something",
	}
	code := command.Run([]string{})
	spec.Expect(code).ToEqual(0)

}

// TODO Test confirm is agreed
// TODO Test confirm is rejected
// TODO Test pre chain commands
// TODO Test inline script handling
// TODO Test intercept of ctrl-c
