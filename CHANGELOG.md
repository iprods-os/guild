# guild

## 0.7.0 (development version)

* Add init command (#1)
* Add support for project variables

## 0.6.1 (2018-10-12)

* Fix bug where nested tasks with confirm weren't handled correctly

## 0.6.0 (2018-06-24)

* Complete refactoring of the internals to enable further features more easily
* Deprecate internal task in favor of task.internal flag
* Deprecate template.command in favor of template.task
* Add installer for auto completion
* Improve inline script handling by setting it to temp instead of the project 
  directory
* Improve help / noop display
* Add version flag
* Improve version handling

## 0.5.0-dev (2017-09-16)

* Simplify command execution (#34)

## 0.4.1 (2017-08-03)

* Add quiet mode to template command (#27)
* Add quiet mode on project level (#27)

## 0.4.0 (2017-08-01)

* Add help display on error (e.g. no Guildfile present) (#1)
* Improve auto-completion for bash
* Add streaming output improvements (#17)
* Add autocompletion for fish (#23)
* Add quiet mode to omit Guild produced output (#26)

## 0.3.0

* Add support of arguments for dependent tasks
* Add internal template tasks
* Add rudimentary timer
* Simplify template tasks to use table arrays
* Improve inline script detection
* Add confirmation for tasks and template tasks
* Add project name per definition
* Remove leading whitespace on inline scripts to allow indentation

## 0.2.0

* Enhance template tasks
* Add command substitution support
* Change temporary script paths
* Refactor and cleanup code base a bit

## 0.1.0

* Add support for inline scripts
* Add task templates
* Add platform dependent tasks
* Add detection of command with shell operators
* Add internal tasks
* Add environment variable support
* Add dependent tasks
* Add basic support for auto completion
* Add colored error output
* Add support for complex commands
