package cli

import (
	"os"

	"github.com/mitchellh/cli"
	"gitlab.com/iprods-os/guild/command"
	"gitlab.com/iprods-os/guild/task"
	"gitlab.com/iprods-os/guild/version"
)

// This var is used to be able to display the project name in the help output.
// TODO Check how to solve more elegant
var projectName string

// Commands factory providing all available commands
func Commands() (map[string]cli.CommandFactory, error) {
	bui := &cli.BasicUi{
		Reader:      os.Stdin,
		Writer:      os.Stdout,
		ErrorWriter: os.Stderr,
	}
	ui := &cli.ColoredUi{
		InfoColor:  cli.UiColorGreen,
		WarnColor:  cli.UiColorYellow,
		ErrorColor: cli.UiColorRed,
		Ui:         bui,
	}
	factory := make(map[string]cli.CommandFactory)
	factory["_version"] = func() (cli.Command, error) {
		return &command.VersionCommand{
			HumanReadableVersion: version.HumanReadableVersion(),
			UI:                   ui,
		}, nil
	}
	// Detect if a Guildfile is present at all and offer to create it
	if _, err := os.Stat("Guildfile"); os.IsNotExist(err) {
		factory["init"] = func() (cli.Command, error) {
			return &command.InitCommand{
				UI: ui,
			}, nil
		}
		return factory, nil
	}
	project, err := task.NewProject(*task.NewLoader("Guildfile"))
	if err != nil {
		return factory, err
	}
	projectName = project.Name
	builder, err := task.NewBuilder(*project)
	if err != nil {
		return factory, err
	}
	for name, guildCommand := range builder.Commands {
		if guildCommand.Internal {
			continue
		}
		description := guildCommand.Description
		cmd := guildCommand
		factory[name] = func() (cli.Command, error) {
			return &command.GuildCommand{
				Description: description,
				Command:     cmd,
				UI:          ui,
			}, nil
		}
	}
	return factory, nil
}
