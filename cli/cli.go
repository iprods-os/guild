package cli

import (
	"fmt"
	"strings"
	"sync"

	"github.com/armon/go-radix"
	"github.com/mitchellh/cli"
	"github.com/posener/complete"
	"github.com/posener/complete/cmd/install"
)

// autocompleteInstaller is an interface to be implemented to perform the
// autocomplete installation and uninstallation with a CLI.
//
// This interface is not exported because it only exists for unit tests
// to be able to test that the installation is called properly.
type autocompleteInstaller interface {
	Install(string) error
	Uninstall(string) error
}

type realAutocompleteInstaller struct{}

func (i *realAutocompleteInstaller) Install(cmd string) error {
	return install.Install(cmd)
}

func (i *realAutocompleteInstaller) Uninstall(cmd string) error {
	return install.Uninstall(cmd)
}

// Decorator for mitchellh/cli in order to inject adjusted autocompletion.
// The main differences is that we want to filter out commands from autocompletion by convention and do not have
// single dash flags for version and help.
// This is more or less a poor man's attempt to overwrite/rebuild the autocompletion functionality of mitchellh/cli.
// It's a lot copy and paste and will not be fun to keep up to date.
// The commandTree therefore is built twice as we cannot access the CLI internal one.
type GuildCli struct {
	cli *cli.CLI

	// Autocomplete fields
	Autocomplete            bool
	AutocompleteInstall     string
	AutocompleteUninstall   string
	AutocompleteGlobalFlags complete.Flags
	autocompleteInstaller   autocompleteInstaller // For tests

	// Internal fields
	once         sync.Once
	autocomplete *complete.Complete
	commandTree  *radix.Tree

	isAutocompleteInstall   bool
	isAutocompleteUninstall bool
}

func (c *GuildCli) Run() (int, error) {
	c.once.Do(c.init)

	// If this is a autocompletion request, handle it. This is set on top of the CLI library to act like we want.
	if c.Autocomplete && c.autocomplete.Complete() {
		return 0, nil
	}

	// If we're attempting to install or uninstall autocomplete then handle
	if c.Autocomplete {
		// Autocomplete requires the "Name" to be set so that we know what
		// command to setup the autocomplete on.
		if c.cli.Name == "" {
			return 1, fmt.Errorf(
				"internal error: CLI.Name must be specified for autocomplete to work")
		}

		// If both install and uninstall flags are specified, then error
		if c.isAutocompleteInstall && c.isAutocompleteUninstall {
			return 1, fmt.Errorf(
				"Either the autocomplete install or uninstall flag may " +
					"be specified, but not both.")
		}

		// If the install flag is specified, perform the install or uninstall
		if c.isAutocompleteInstall {
			fmt.Println("Installing")
			if err := c.autocompleteInstaller.Install(c.cli.Name); err != nil {
				return 1, err
			}

			return 0, nil
		}

		if c.isAutocompleteUninstall {
			if err := c.autocompleteInstaller.Uninstall(c.cli.Name); err != nil {
				return 1, err
			}

			return 0, nil
		}
	}

	return c.cli.Run()
}

func (c *GuildCli) init() {
	// Turn off CLI autocompletion
	c.cli.Autocomplete = false
	// Check for guildCli autocomplete flags
	if c.Autocomplete {
		// Build the command tree
		c.commandTree = radix.New()
		for k, v := range c.cli.Commands {
			k = strings.TrimSpace(k)
			c.commandTree.Insert(k, v)
		}
		c.initAutocomplete()
		for _, arg := range c.cli.Args {
			if arg == "--" {
				break
			}

			if arg == "-"+c.AutocompleteInstall || arg == "--"+c.AutocompleteInstall {
				c.isAutocompleteInstall = true
				continue
			}

			if arg == "-"+c.AutocompleteUninstall || arg == "--"+c.AutocompleteUninstall {
				c.isAutocompleteUninstall = true
				continue
			}
		}
	}
}

func (c *GuildCli) initAutocomplete() {
	if c.AutocompleteInstall == "" {
		c.AutocompleteInstall = defaultAutocompleteInstall
	}

	if c.AutocompleteUninstall == "" {
		c.AutocompleteUninstall = defaultAutocompleteUninstall
	}

	if c.autocompleteInstaller == nil {
		c.autocompleteInstaller = &realAutocompleteInstaller{}
	}

	// Build the root command
	cmd := c.initAutocompleteSub("")

	// For the root, we add the global flags to the "Flags". This way
	// they don't show up on every command.
	cmd.Flags = map[string]complete.Predictor{
		"--" + c.AutocompleteInstall:   complete.PredictNothing,
		"--" + c.AutocompleteUninstall: complete.PredictNothing,
		"--help":                       complete.PredictNothing,
		"--version":                    complete.PredictNothing,
	}
	cmd.GlobalFlags = c.AutocompleteGlobalFlags

	c.autocomplete = complete.New(c.cli.Name, cmd)
}

// initAutocompleteSub creates the complete.Command for a subcommand with
// the given prefix. This will continue recursively for all subcommands.
// The prefix "" (empty string) can be used for the root command.
func (c *GuildCli) initAutocompleteSub(prefix string) complete.Command {
	var cmd complete.Command
	walkFn := func(k string, raw interface{}) bool {
		if len(prefix) > 0 {
			// If we have a prefix, trim the prefix + 1 (for the space)
			// Example: turns "sub one" to "one" with prefix "sub"
			k = k[len(prefix)+1:]
		}

		// Keep track of the full key so that we can nest further if necessary
		fullKey := k

		// guild internal commands should not get autocompleted as those are mapped to flags.
		if strings.Index(fullKey, "_") == 0 {
			return false
		}

		if idx := strings.LastIndex(k, " "); idx >= 0 {
			// If there is a space, we trim up to the space
			k = k[:idx]
		}

		if idx := strings.LastIndex(k, " "); idx >= 0 {
			// This catches the scenario just in case where we see "sub one"
			// before "sub". This will let us properly setup the subcommand
			// regardless.
			k = k[idx+1:]
		}

		if _, ok := cmd.Sub[k]; ok {
			// If we already tracked this subcommand then ignore
			return false
		}

		if cmd.Sub == nil {
			cmd.Sub = complete.Commands(make(map[string]complete.Command))
		}
		subCmd := c.initAutocompleteSub(fullKey)

		// Instantiate the command so that we can check if the command is
		// a CommandAutocomplete implementation. If there is an error
		// creating the command, we just ignore it since that will be caught
		// later.
		impl, err := raw.(cli.CommandFactory)()
		if err != nil {
			impl = nil
		}

		// Check if it implements ComandAutocomplete. If so, setup the autocomplete
		if c, ok := impl.(cli.CommandAutocomplete); ok {
			subCmd.Args = c.AutocompleteArgs()
			subCmd.Flags = c.AutocompleteFlags()
		}

		cmd.Sub[k] = subCmd
		return false
	}

	walkPrefix := prefix
	if walkPrefix != "" {
		walkPrefix += " "
	}
	c.commandTree.WalkPrefix(walkPrefix, walkFn)
	return cmd
}

// defaultAutocompleteInstall and defaultAutocompleteUninstall are the
// default values for the autocomplete install and uninstall flags.
const defaultAutocompleteInstall = "autocomplete-install"
const defaultAutocompleteUninstall = "autocomplete-uninstall"
