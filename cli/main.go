package cli

import (
	"fmt"
	"os"

	"github.com/mitchellh/cli"
)

// Run the program.
func Run(args []string) int {
	commandFactory, err := Commands()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed loading project: %s", err.Error())
		return 1
	}
	return RunCustom(args, commandFactory)
}

// RunCustom inits the app, registers default commands and task definitions as well as auto-complete.
func RunCustom(args []string, commands map[string]cli.CommandFactory) int {
	filteredArgs := []string{}
	for _, arg := range args {
		// Guild commands often have the --help flag themselves.
		// So we ignore this for the underlying CLI library.
		if arg == "--help" {
			continue
		}
		filteredArgs = append(filteredArgs, arg)
		// TODO Place to add guild internal flags like f.e. validate a Guildfile etc.
		if arg == "--" {
			break
		}
		// Map version flags to the version command
		if arg == "-v" || arg == "--version" {
			filteredArgs = append(filteredArgs, "_version")
			break
		}
	}
	commandsInclude := make([]string, len(commands))
	for k := range commands {
		// Filter out flagged command from help display
		if k == "_version" {
			continue
		}
		commandsInclude = append(commandsInclude, k)
	}
	guildCli := &GuildCli{
		cli: &cli.CLI{
			Name:     "guild",
			Args:     filteredArgs,
			Commands: commands,
			HelpFunc: cli.FilteredHelpFunc(commandsInclude, Help),
		},
		Autocomplete: true,
	}
	exitCode, err := guildCli.Run()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error executing CLI: %s\n", err.Error())
		return 1
	}

	return exitCode
}
