default: build

build: clean
	@go fmt ./... && go build -o build/guild -v

release: clean
	@go build -o build/macos/guild
	@GOOS=linux GOARCH=386 CGO_ENABLED=0 go build -o build/linux/guild
	@GOOS=windows GOARCH=386 CGO_ENABLED=0 go build -o build/windows/guild.exe

clean:
	@go clean

test:
	@go test ./... -v

test-cover:
	@go test ./... -v -cover
